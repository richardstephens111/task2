/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package task2programming;
import java.util.*;
/**
 *used **** where there was a problem, steps were put in to see how far
 * process went before breaking.
 * @author User-Admin
 */
public class Task2Programming {
        public static void main(String[] args) {
    //table creation
        String[][] grid = createGrid();
        //loop to keep playing
        boolean loop = true;
        
        int count =0;
        printGrid(grid);
        //loop for turns
        while (loop)
        {
           // ****System.out.println("start");****
            //Oplayer to go first
            if (count %2 == 0)OPattern(grid);
            else XPattern(grid);
            count++;//counts turns
            //****System.out.println(loop);*****
            printGrid(grid);
            //****System.out.println("check winner is: "+checkWinner(grid));****
           
            //checks winner after every turn
        if (checkWinner(grid) != null)
        {
            
            if (checkWinner(grid) == "X" )
            System.out.println("Congratulations, player X has won the game");
            else if (checkWinner(grid) == "O")
            System.out.println("Congratulations, player O has won the game ");
            //****else if (checkWinner(grid)== " ")****
                //****System.out.println("'-' has won the game");****
            
            loop = false;
          //  ****System.out.println("winner");****
        }
           // ****System.out.println("loop end");****
        }
    }

public static String [][] createGrid()
        //creates the connect4 grid/board
{
    String [][] grid = new String [7][15];
    //grid size of 2d array 7rows,15 columns was chosen 
    //extra row to show _ at the bottom 
    //between each column has || so doubled amount
    for (int i=0;i<grid.length;i++)
        //loops each row
    {
    for (int j=0;j<grid[i].length;j++) 
        //loops each column
    {
        if (j% 2== 0 )grid[i][j] = "|";
        else grid[i][j] = " ";
          //even number to have border
        //odd number to have number or space
        
        if (i == 6) grid[i][j] = "-";
      //bottom row
    }
        
    
    
}
    return grid;
}
    public static void printGrid(String[][] grid)
    //viewing grid
    {
     for (int i =0;i<grid.length;i++)  
     {
         for (int j = 0;j<grid[i].length;j++)
         {
             System.out.print(grid[i][j]);
             
         }
         System.out.println();
     }
    }

    //lowest empty row will inherit a O
    public static void OPattern(String[][]grid)
    //
    {
    //asks user what column to put the O into.
    System.out.println("Drop O at coloumn (0-6): ");
    Scanner input = new Scanner(System.in);
    //converts 1-2-3-4-5-6 into columns 1-3-5-7-9-11-13
    int col = 2*input.nextInt()+1;
    
    for (int i=5;i>=0;i--)
        //loop each row to find empty space to place O
    {
        if (grid[i][col] == " ")
        {
            grid[i][col] = "O";
            break;
        }
    }
}
    public static void XPattern(String[][]grid)
    {
         System.out.println("Drop X at coloumn (0-6): ");
    Scanner input = new Scanner(System.in);
    int col = 2*input.nextInt()+1;
    
    for (int i=5;i>=0;i--)
    {
        if (grid[i][col] == " ")
        {
            grid[i][col] = "X";
            break;
        }
    }
}
    //checking patterns of 4 for winners
    //horizontal,vertical,diagonal left up to right down 
    //and diagonal left down to right up
    public static String checkWinner(String[][] grid)
    {
        //loops from 0-5 looking for winning horizontal line
        for (int i=0;i<6;i++)
        {
            //incremented by 2
            for (int j=0;j<7;j+=2)
            {
                if ((grid[i][j+1] != " ")
                && (grid[i][j+3] != " ") 
                && (grid[i][j+5] != " ") 
                && (grid[i][j+7] != " ") 
                && ((grid[i][j+1] == grid[i][j+3]) 
                && (grid[i][j+3] == grid[i][j+5]) 
                && (grid[i][j+5] == grid[i][j+7])))
                { 
                   // ****System.out.println("path1");****
                    
                //returns same value to check win X or O 
                    return grid[i][j+1];
                }
            }
        }
        for (int i=1;i<15;i+=2)
    //increments vertical by 2 looping over each odd column
    //looking for same X or O
        {
            for (int j=0;j<3;j++)
            {
                if ((grid[j][i] != " ") 
                && (grid[j+1][i] != " ") 
                && (grid[j+2][i] != " ") 
                && (grid[j+3][i] != " ") 
                && ((grid[j][i] == grid[j+1][i]) 
                && (grid[j+1][i] == grid[j+2][i]) 
                && (grid[j+2][i] == grid[j+3][i])))
                {
                    //****System.out.println("path2");****
                    return grid[j][i];
                }
            }
        }
    
        for (int i=0;i<3;i++)
            //checking diagonal line left up to down right
        {
            for (int j=1;j<9;j+=2)   
            {
                if ((grid[i][j] != " ")
                && (grid[i+1][j+2] != " ") 
                && (grid[i+2][j+4] != " ") 
                && (grid[i+3][j+6] != " ") 
                && ((grid[i][j] == grid[i+1][j+2]) 
                && (grid[i+1][j+2] == grid[i+2][j+4]) 
                && (grid[i+2][j+4] == grid[i+3][j+6]))) 
                {   
                   // ****System.out.println("path3");****
                    return grid[i][j];
                }
            }
        }
    
        for (int i=0;i<3;i++)
        //reversing check top right to left bottom
        {
            for (int j=7;j<15;j+=2)
            {
            if ((grid[i][j] != " ")
                && (grid[i+1][j-2] != " ") 
                && (grid[i+2][j-4] != " ") 
                && (grid[i+3][j-6] != " ") 
                && ((grid[i][j] == grid[i+1][j-2]) 
                && (grid[i+1][j-2] == grid[i+2][j-4]) 
                && (grid[i+2][j-4] == grid[i+3][j-6]))) 
            {
               // ****System.out.println("path4");****
                return grid[i][j];
            }
            }
        }
       // ****System.out.println("returning null");****
        return null;
        //returns if no same X or O
    }
}
    

